#!/usr/bin/env node

process.chdir(__dirname)
process.chdir('..')

var fs = require('fs')
var uglifyJs = require('uglify-js')

var files = [
    'js/Blink.js',
    'js/Field.js',
    'js/Div.js',
    'js/MainPanel.js',
    'js/MuteButton.js',
    'js/TapButton.js',
    'js/TextNode.js',
    'js/Main.js',
]

var source = '(function () {\n'
files.forEach(function (file) {
    source += fs.readFileSync(file, 'utf8') + ';\n'
})
source += '\n})()'

var compressedSource = uglifyJs.minify({ 'combined.js': source }).code

fs.writeFileSync('combined.js', source)
fs.writeFileSync('compressed.js', compressedSource)
